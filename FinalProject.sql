DROP DATABASE IF EXISTS ECommerce_Delivery;
CREATE DATABASE ECommerce_Delivery;
USE ECommerce_Delivery;

CREATE TABLE Utente(
    id INTEGER NOT NULL AUTO_INCREMENT,
    nominativo VARCHAR(100) NOT NULL,
    nomeUtente VARCHAR(100) NOT NULL UNIQUE,
    pass VARCHAR(100) NOT NULL,
    ruolo VARCHAR(100) CONSTRAINT CHECK (ruolo IN ("ADMIN", "STORE", "CLIENTE")),
    indirizzoSpedizione VARCHAR(250) DEFAULT (""),
    indirizzoFatturazione VARCHAR(250) DEFAULT (""),
    email VARCHAR(250) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

CREATE TABLE Oggetto(
    id INTEGER NOT NULL AUTO_INCREMENT,
    nome VARCHAR(100) NOT NULL,
    codice VARCHAR(20) NOT NULL UNIQUE,
    quantita INTEGER DEFAULT 0,
    prezzo FLOAT NOT NULL CONSTRAINT CHECK (prezzo >0),
    descrizione TEXT DEFAULT (""),
    PRIMARY KEY (id)
);

CREATE TABLE Ordine(
    id INTEGER NOT NULL AUTO_INCREMENT,
    codiceOrdine VARCHAR(20) NOT NULL UNIQUE,
    totale FLOAT NOT NULL,
    dataOrdine TIMESTAMP,
    clienteId INTEGER NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (clienteId) REFERENCES Utente(id)
);

CREATE TABLE OggettoOrdine(
    oggettoId INTEGER NOT NULL,
    ordineId INTEGER NOT NULL,
    FOREIGN KEY (oggettoId) REFERENCES Oggetto(id),
    FOREIGN KEY (ordineId) REFERENCES Ordine(id) ON DELETE CASCADE
);

CREATE TABLE OggettoStore(
    oggettoId INTEGER NOT NULL,
    storeId INTEGER NOT NULL,
    PRIMARY KEY (oggettoId, storeId),
    FOREIGN KEY (oggettoId) REFERENCES Oggetto(id) ON DELETE CASCADE,
    FOREIGN KEY (storeId) REFERENCES Utente(id) ON DELETE CASCADE
);
-- DROP TABLE OggettoStore;

insert into Oggetto (nome, codice, quantita, prezzo, descrizione) VALUES 
("iphone","IP1292912",54,1329.99,"Non vale quel che costa"),
("Matebook","HM123",10, 799.99, "Notebook molto leggero"),
("Scrivania","SCRI123",12, 140, "Scrivania facile da montare"),
("Pasta ripiena","PRI123",33, 2.12, "Delicatissimi ravioli ripieni di ricotta e spinaci"),
("Acqua Martina","ACMT123",100, 1.1, "Acqua economica"),
("Asus ROG","AROG123",23, 1538.23, "Notebook da gaming"),
("MacBook Pro","MBP123",37, 2519.99, "Aura potentissima"),
("Lampada bella","LMP123",23, 15.92, "Lampada da scrivania"),
("Mouse","MRGX123",33, 56.12, "Mouse da gaming con led"),
("Divano","DIV123",3, 1581, "Comodo divano in pelle"),
("Sedia","Chair123",200, 42.2, "Sedia per sedersi");

insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Irvine', 'istanggjertsen0', 'ibisp0@histats.com', '75DNVeu', '01 Anniversary Crossing', '330 Park Meadow Terrace', 'Cliente');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Percival', 'pedmed1', 'ppeedell1@forbes.com', '6gyY6Zg3x75', '468 Anhalt Terrace', '6016 Mitchell Point', 'Cliente');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Stephana', 'smcshirrie2', 'sackland2@qq.com', 'Vl2qu0ZXHv', '2339 New Castle Circle', '0835 Scofield Alley', 'Cliente');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Candice', 'clagne3', 'ccatchpole3@cnn.com', 'TDGdnE9c', '10912 Marquette Street', '16 Scoville Trail', 'Cliente');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Mobilig', 'nnormanville4', 'nkeuning4@linkedin.com', 'lbcl9n4', '', '', 'ADMIN');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Finn', 'ftwells5', 'fmalloy5@economist.com', 'gxy7PNTRqjV', '25802 Buhler Place', '146 Westridge Court', 'Cliente');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Glenine', 'gcratere6', 'ghebblethwaite6@lycos.com', 'S8ldrQ7zJiet', '03765 Sundown Court', '846 Spenser Center', 'Cliente');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Arredamenti e allestimenti', 'tlindenberg7', 'tonoulane7@soundcloud.com', 'WmeMJ3seTa', '', '', 'STORE');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Joeann', 'jhedworth8', 'jiglesias8@bluehost.com', 'q3bbrD44', '87082 Eagan Terrace', '1797 Barby Court', 'Cliente');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Bazar', 'kjarrold9', 'kmacmenamy9@blinklist.com', 'hJwCHXby', '', '', 'STORE');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Julieta', 'jcraydona', 'jgrenshieldsa@stanford.edu', 'g1V4s69vM', '07105 Linden Street', '15 Forest Alley', 'Cliente');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Torr', 'tconnochieb', 'tsilversb@jalbum.net', '0Sbv5xZMU', '5 Menomonie Pass', '35051 Cody Road', 'Cliente');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Saturn', 'gsilvermanc', 'gbanbrookc@list-manage.com', 'XHL1g7f5', '', '', 'STORE');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Hervey', 'hbusselld', 'hsimkovitzd@time.com', 'YfWnLS68GS', '', '', 'ADMIN');
insert into Utente (nominativo, nomeUtente, email, pass, indirizzoSpedizione, indirizzoFatturazione, ruolo) values ('Mallory', 'mbotfielde', 'malvise@loc.gov', '8wzAJvxCT', '6415 Village Terrace', '3281 Bowman Park', 'Cliente');

INSERT INTO Ordine (codiceOrdine, totale, dataOrdine, clienteId) VALUES
("123-456", 34, 1/9/2021,5);

INSERT INTO OggettoStore (OggettoId,StoreId) VALUES
(1,13),
(2,13),
(2,10),
(4,10),
(11,8),
(3,8),
(5,8),
(6,13),
(7,13),
(8,8),
(9,13),
(10,8);

INSERT INTO OggettoOrdine (oggettoId, ordineId) VALUES 
(5,1);

SELECT * FROM utente 
	JOIN OggettoStore ON utente.id = OggettoStore.storeid
    JOIN oggetto ON OggettoStore.oggettoId = oggetto.id;
    
SELECT oggetto.id, oggetto.nome, oggetto.codice, oggetto.prezzo, oggetto.descrizione FROM oggetto
	JOIN oggettoordine on oggetto.id = OggettoOrdine.oggettoId
    JOIN ordine on OggettoOrdine.ordineId = ordine.id;
    
SELECT * FROM oggetto
	JOIN oggettoordine on oggetto.id = OggettoOrdine.oggettoId
    JOIN ordine on OggettoOrdine.ordineId = ordine.id;