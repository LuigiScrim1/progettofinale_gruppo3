package com.amministrazione.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amministrazione.model.Ordine;
import com.amministrazione.services.OrdineDAO;
import com.amministrazione.utility.ResponsoOperazione;
import com.google.gson.Gson;


@WebServlet("/ordineeliminazione")
public class OrdineEliminazione extends HttpServlet {

    /**
     * Servlet per l'eliminazione di un ordine tramite ID
     */

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        Gson jsonizzatore = new Gson();

        Integer identificatore = request.getParameter("id_ordine") != null ? Integer.parseInt(request.getParameter("id_ordine")) : -1;

        if(identificatore != -1) {

            OrdineDAO ordDao = new OrdineDAO();
            try {

                Ordine ord = ordDao.getById(identificatore);
                if(ordDao.delete(ord)) {
                    ResponsoOperazione res = new ResponsoOperazione("OK", "");
                    out.print(jsonizzatore.toJson(res));
                }
                else {
                    ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Nono sono riuscito a fare l'eliminazione");
                    out.print(jsonizzatore.toJson(res));
                }

            } catch (SQLException e) {
                ResponsoOperazione res = new ResponsoOperazione("ERRORE", e.getMessage());
                out.print(jsonizzatore.toJson(res));
            }

        }
        else {
            ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Id non selezionato!");
            out.print(jsonizzatore.toJson(res));
        }
    }

}