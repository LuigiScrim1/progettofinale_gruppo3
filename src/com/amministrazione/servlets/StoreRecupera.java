package com.amministrazione.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amministrazione.model.Utente;
import com.amministrazione.services.UtenteDAO;
import com.amministrazione.utility.ResponsoOperazione;
import com.google.gson.Gson;

@WebServlet("/StoreRecupera")
public class StoreRecupera extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        UtenteDAO userDao = new UtenteDAO();

        Integer identificatore = request.getParameter("id_store") != null ? Integer.parseInt(request.getParameter("id_store")) : -1;

        try {

            if(identificatore == -1) {
                ArrayList<Utente> elencoUtenti = userDao.getAll();
                ArrayList<Utente> elencoStore = new ArrayList<Utente>();

                for (int i=0; i<elencoUtenti.size(); i++) {

                    if(elencoUtenti.get(i).getRuolo().equals( "STORE")) {
                        elencoStore.add(elencoUtenti.get(i));
                    }
                }
                String risultatoJson = new Gson().toJson(elencoStore);
                Thread.sleep(500);
                out.print(risultatoJson);
            }
            else {
                Utente temp = userDao.getById(identificatore);
                    if(temp.getRuolo().equals("STORE")) {
                        out.print(new Gson().toJson(temp));
                    }
                    else {
                        out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "L'ID inserito non corrisonde ad uno store")));
                    }
            }

        } catch (SQLException | InterruptedException e) {

            out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));
            System.out.println(e.getMessage());

        }

    }

}