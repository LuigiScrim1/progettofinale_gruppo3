package com.amministrazione.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.amministrazione.model.Utente;
import com.amministrazione.services.UtenteDAO;
import com.amministrazione.utility.ResponsoOperazione;
import com.google.gson.Gson;


@WebServlet("/effettualogin")
public class EffettuaLogin extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        ResponsoOperazione res = new ResponsoOperazione("Errore", "Metodo non permesso!");
        out.print(new Gson().toJson(res));
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        String varUtente = request.getParameter("inputUserL") != null ? request.getParameter("inputUserL") : "";
        String varPass = request.getParameter("inputPasswordL") != null ? request.getParameter("inputPasswordL") : "";

        if(varUtente.isBlank() || varPass.isBlank()) {
            ResponsoOperazione res = new ResponsoOperazione("Errore", "Inserisci campi user e password");
            out.print(new Gson().toJson(res));
            return;
        }

        Utente utente = new Utente();
        utente.setNomeUtente(varUtente);
        utente.setPass(varPass);

        HttpSession sessione = request.getSession();

        UtenteDAO uDao = new UtenteDAO();

        try {
            if(uDao.controllaUtente(utente)) {
                if(utente.getRuolo().equalsIgnoreCase("ADMIN")) {
                    sessione.setAttribute("isAuth", "true");
                    sessione.setAttribute("ruolo", utente.getRuolo());
                    sessione.setAttribute("user", utente.getNomeUtente());
                    response.sendRedirect("admin.jsp");
                }
                else {
                    ResponsoOperazione res = new ResponsoOperazione("Errore", "Credenziali non di amministratore");
                    out.print(new Gson().toJson(res));
                }
            }
            else {
                ResponsoOperazione res = new ResponsoOperazione("Errore", "Credenziali non valide");
                out.print(new Gson().toJson(res));
            }
        } catch (SQLException e) {
            ResponsoOperazione res = new ResponsoOperazione("Errore", "Errore recupero informazioni");
            out.print(new Gson().toJson(res));
            System.out.println(e.getMessage());
        }
    }
}
