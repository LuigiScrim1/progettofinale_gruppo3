package com.amministrazione.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amministrazione.model.Ordine;
import com.amministrazione.model.Utente;
import com.amministrazione.services.OrdineDAO;
import com.amministrazione.services.UtenteDAO;
import com.amministrazione.utility.ResponsoOperazione;
import com.google.gson.Gson;

@WebServlet("/ordinerecupera")
public class OrdineRecupera extends HttpServlet {

    /**
     * Servlet per la lettura di un singolo ordine se inserisco l'ID ad esso relativo o l'intera lista in caso contrario
     */

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        OrdineDAO ordDao = new OrdineDAO();
        Utente cliente = new Utente();
        UtenteDAO uDAO = new UtenteDAO();

        Integer identificatore = request.getParameter("id_ordine") != null ? Integer.parseInt(request.getParameter("id_ordine")) : -1;

        try {

            if(identificatore == -1) {
                ArrayList<Ordine> elencoOrdini = ordDao.getAll();
                String risultatoJson = new Gson().toJson(elencoOrdini);
                Thread.sleep(500);
                out.print(risultatoJson);
            }
            else {
                Ordine temp = ordDao.getById(identificatore);
                out.print(new Gson().toJson(temp));
            }

        } catch (SQLException | InterruptedException e) {

            out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage())));
            System.out.println(e.getMessage());

        }
    }

}