package com.amministrazione.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.amministrazione.model.Utente;
import com.amministrazione.services.UtenteDAO;
import com.amministrazione.utility.ResponsoOperazione;
import com.google.gson.Gson;

@WebServlet("/storeeliminazione")
public class StoreEliminazione extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        Gson jsonizzatore = new Gson();

        Integer identificatore = request.getParameter("id_store") != null ? Integer.parseInt(request.getParameter("id_store")) : -1;

        if(identificatore != -1) {

            UtenteDAO userDao = new UtenteDAO();
            try {

                Utente temp = userDao.getById(identificatore);
                if(temp.getRuolo().equals("STORE")) {

                        if(userDao.delete(temp)) {
                            ResponsoOperazione res = new ResponsoOperazione("OK", "");
                            out.print(jsonizzatore.toJson(res));
                        }
                        else {
                            ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Nono sono riuscito a fare l'eliminazione");
                            out.print(jsonizzatore.toJson(res));
                        }
                }

                else {
                    out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "L'ID inserito non corrisonde ad uno store")));
                }
            } catch (SQLException e) {
                ResponsoOperazione res = new ResponsoOperazione("ERRORE", e.getMessage());
                out.print(jsonizzatore.toJson(res));
            }

        }
        else {
            ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Id non selezionato!");
            out.print(jsonizzatore.toJson(res));
        }

    }

}
