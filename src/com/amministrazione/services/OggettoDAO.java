package com.amministrazione.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.amministrazione.connessione.ConnettoreDB;
import com.amministrazione.model.Oggetto;
import com.amministrazione.model.Ordine;
import com.amministrazione.model.Utente;

public class OggettoDAO implements Dao<Oggetto>{

public boolean insertRelazione(Oggetto t, Utente store) throws SQLException {
    boolean risultato = false;
    Connection conn = ConnettoreDB.getIstanza().getConnessione();

    String query = "INSERT INTO OggettoStore (idOggetto, idStore) VALUE (?,?)";             
    PreparedStatement ps =  conn.prepareStatement(query);
    ps.setInt(1, t.getId());
    ps.setInt(2, store.getId());

    int rowAffected = ps.executeUpdate();
    if(rowAffected>0)
        risultato = true;       
    return risultato;

}

/**

 * Metodo che ritorna un oggetto ricercato nel DB tramite ID
 */
@Override
public Oggetto getById(int id) throws SQLException {

    Connection conn = ConnettoreDB.getIstanza().getConnessione();

    String query = "SELECT id, nome, codice, quantita, prezzo, descrizione FROM oggetto WHERE id = ?";
    PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
    ps.setInt(1, id);

    ResultSet risultato = ps.executeQuery();
    risultato.next();

    Oggetto temp = new Oggetto();
    temp.setId(risultato.getInt(1));
    temp.setNome(risultato.getString(2));
    temp.setCodice(risultato.getString(3));
    temp.setQuantita(risultato.getInt(4));
    temp.setPrezzo(risultato.getFloat(5));
    temp.setDescrizione(risultato.getString(6));
    return temp;

}

/**

 * Metodo che ritorna l'ArrayList di tutti gli oggetti presenti nel DB
 */
@Override
public ArrayList<Oggetto> getAll() throws SQLException {

    ArrayList<Oggetto> elenco = new ArrayList<Oggetto>();

    Connection conn = ConnettoreDB.getIstanza().getConnessione();

    String query = "SELECT id, nome, codice, quantita, prezzo, descrizione FROM oggetto";
    PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

    ResultSet risultato = ps.executeQuery();
    while(risultato.next()){
        Oggetto temp = new Oggetto();
        temp.setId(risultato.getInt(1));
        temp.setNome(risultato.getString(2));
        temp.setCodice(risultato.getString(3));
        temp.setQuantita(risultato.getInt(4));
        temp.setPrezzo(risultato.getFloat(5));
        temp.setDescrizione(risultato.getString(6));
        elenco.add(temp);
    }

    return elenco;
}

/**

 * Funzione che permette l'inserimento di un nuovo oggetto nel DB
 */
@Override
public void insert(Oggetto t) throws SQLException {

    Connection conn = ConnettoreDB.getIstanza().getConnessione();


    String query = "INSERT INTO oggetto (nome, codice, quantita, prezzo, descrizione) VALUES (?,?,?,?,?)";
    PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
    ps.setString(1, t.getNome());
    ps.setString(2, t.getCodice());
    ps.setInt(3, t.getQuantita());
    ps.setFloat(4, t.getPrezzo());
    ps.setString(5, t.getDescrizione());

    ps.executeUpdate();
    ResultSet risultato = ps.getGeneratedKeys();
    risultato.next();

    t.setId(risultato.getInt(1));

}

/**

 * Funzione che consente di eliminare un oggetto tramite ID
 */
@Override
public boolean delete(Oggetto t) throws SQLException {

    Connection conn = ConnettoreDB.getIstanza().getConnessione();
    String query = "DELETE FROM oggetto WHERE id = ?";
    PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
    ps.setInt(1, t.getId());

    int affRows = ps.executeUpdate();
    if(affRows > 0)
        return true;

    return false;

}

/**

 * Funzione che permette di modificare un oggetto (cercato per id) e ritorna true o false a seconda che l'operazione sia andata a buon fine o meno
 */
@Override
public boolean update(Oggetto t) throws SQLException {

    Connection conn = ConnettoreDB.getIstanza().getConnessione();
    String query = "UPDATE oggetto SET "

            + "nome = ?, "
            + "codice = ?, "
            + "quantita = ?, "
            + "prezzo = ?, "
            + "descrizione = ? "
            + "WHERE id = ?";
    PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
    ps.setString(1, t.getNome());
    ps.setString(2, t.getCodice());
    ps.setInt(3, t.getQuantita());
    ps.setFloat(4, t.getPrezzo());
    ps.setString(5, t.getDescrizione());
    ps.setInt(6, t.getId());

    int affRows = ps.executeUpdate();
    if(affRows > 0)
        return true;

    return false;
}

/**

 * Funzione che permette di modificare un oggetto (cercato per id) e ritorna l'oggetto modificato
 * @param t oggetto da modificare
 * @return oggetto modificato
 * @throws SQLException
 */
public Oggetto updateWithReturn(Oggetto t) throws SQLException {

    Connection conn = ConnettoreDB.getIstanza().getConnessione();
    String query = "UPDATE oggetto SET "

            + "nome = ?, "
            + "codice = ?, "
            + "quantita = ?, "
            + "prezzo = ?, "
            + "descrizione = ? "
            + "WHERE id = ?";
    PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
    ps.setString(1, t.getNome());
    ps.setString(2, t.getCodice());
    ps.setInt(3, t.getQuantita());
    ps.setFloat(4, t.getPrezzo());
    ps.setString(5, t.getDescrizione());
    ps.setInt(6, t.getId());

    int affRows = ps.executeUpdate();
    if(affRows > 0)
        return getById(t.getId());

    return null;
}

/**

 *  Metodo che ritorna un oggetto ricercato nel DB tramite codice
 * @param varCodice Codice oggetto da ricercare
 * @return oggetto cercato
 * @throws SQLException
 */
public Oggetto getByCodice(String varCodice) throws SQLException {

    Connection conn = ConnettoreDB.getIstanza().getConnessione();

    String query = "SELECT id, nome, codice, quantita, prezzo, descrizione FROM oggetto WHERE codice = ?";
    PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
    ps.setString(1, varCodice);

    ResultSet risultato = ps.executeQuery();
    risultato.next();

    Oggetto temp = new Oggetto();
    temp.setId(risultato.getInt(1));
    temp.setNome(risultato.getString(2));
    temp.setCodice(risultato.getString(3));
    temp.setQuantita(risultato.getInt(4));
    temp.setPrezzo(risultato.getFloat(5));
    temp.setDescrizione(risultato.getString(6));
    return temp;

}
}