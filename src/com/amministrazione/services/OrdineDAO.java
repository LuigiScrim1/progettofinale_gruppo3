package com.amministrazione.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.amministrazione.model.Oggetto;
import com.amministrazione.model.Ordine;
import com.amministrazione.model.Utente;
import com.amministrazione.connessione.ConnettoreDB;

public class OrdineDAO implements Dao<Ordine>{
	
	
	public ArrayList<Oggetto> trovaOggetti(int id)throws SQLException{
		ArrayList<Oggetto> elencoArticoli = new ArrayList<Oggetto>();
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "SELECT oggetto.id, oggetto.nome, oggetto.codice, oggetto.prezzo, oggetto.descrizione FROM oggetto"
				+ "	JOIN oggettoordine on oggetto.id = OggettoOrdine.oggettoId"
				+ " JOIN ordine on OggettoOrdine.ordineId = ordine.id"
				+ " WHERE ordine.id = ?";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setInt(1, id);

        ResultSet risultato = ps.executeQuery();
        while(risultato.next()) {
            Oggetto temp = new Oggetto();
            temp.setId(risultato.getInt(1));
            temp.setNome(risultato.getString(2));
            temp.setCodice(risultato.getString(3));
            temp.setPrezzo(risultato.getFloat(4));
            temp.setDescrizione(risultato.getString(5));
            elencoArticoli.add(temp);
        }
		return elencoArticoli;
	}

	@Override
	public Ordine getById(int id) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		
		String query = "SELECT id, codiceOrdine, totale, dataOrdine, clienteId FROM Ordine WHERE id = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setInt(1, id);

        ResultSet risultato = ps.executeQuery();
        risultato.next();
        Ordine temp = new Ordine();
        temp.setId(risultato.getInt(1));
        temp.setCodiceOrdine(risultato.getString(2));
        temp.setTotale(risultato.getFloat(3));
        temp.setDataOrdine(risultato.getTimestamp(4).toLocalDateTime());
        temp.setClientRef(risultato.getInt(5));
        temp.setArray_oggetti(trovaOggetti(id));

        return temp;
	}

	@Override
	public ArrayList<Ordine> getAll() throws SQLException {
		ArrayList<Ordine> elenco = new ArrayList<Ordine>();
        Utente cliente = new Utente();
        UtenteDAO uDAO = new UtenteDAO();

        Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "SELECT id, codiceOrdine, totale, dataOrdine, clienteId FROM Ordine";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

        ResultSet risultato = ps.executeQuery();
        while(risultato.next()){
        	Ordine temp = new Ordine();
        	temp.setId(risultato.getInt(1));
            temp.setCodiceOrdine(risultato.getString(2));
            temp.setTotale(risultato.getFloat(3));
            temp.setDataOrdine(risultato.getTimestamp(4).toLocalDateTime());
            temp.setNomeCliente(uDAO.getById(risultato.getInt(5)).getNominativo());
            temp.setArray_oggetti(trovaOggetti(temp.getId()));
            elenco.add(temp);
            
        }
        return elenco;
	}

	@Override
	public void insert(Ordine t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "INSERT INTO Ordine (codiceOrdine, totale, dataOrdine, clienteId) VALUES (?,?,?,?)";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, t.getCodiceOrdine());
        ps.setFloat(2, t.getTotale());
        ps.setTimestamp(3, Timestamp.valueOf(t.getDataOrdine()));
        ps.setInt(4, t.getClientRef());

        ps.executeUpdate();
        ResultSet risultato = ps.getGeneratedKeys();
        risultato.next();

        t.setId(risultato.getInt(1));
	}
	
	public boolean insertRelazione(Ordine t, ArrayList<Oggetto> elenco) throws SQLException {
		int varControllo = 0;
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		for(int i=0; i<elenco.size(); i++) {
			String query = "INSERT INTO oggettoordine (oggettoId, ordineId) VALUES (?,?)";
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
			ps.setInt(1,t.getId());
			ps.setInt(2, elenco.get(i).getId());
			int rowAffected = ps.executeUpdate();
	        if(rowAffected>0)
	        	varControllo++;
		}	
		if(varControllo==elenco.size())
			return true;
		return false;
	}

	@Override
	public boolean delete(Ordine t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        String query = "DELETE FROM Ordine WHERE id = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setInt(1, t.getId());

        int affRows = ps.executeUpdate();
        if(affRows > 0)
            return true;

        return false;
	}

	@Override
	public boolean update(Ordine t) throws SQLException {
		
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
        String query = "UPDATE Ordine SET "
                + "codiceOrdine = ?, "
                + "totale = ?, "
                + "dataOrdine = ? "
                + "clienteId = ?"
                + "WHERE id = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setString(1, t.getCodiceOrdine());
        ps.setFloat(2, t.getTotale());
        ps.setTimestamp(3, Timestamp.valueOf(t.getDataOrdine()));
        ps.setInt(4, t.getClientRef());
        ps.setInt(5, t.getId());

        int affRows = ps.executeUpdate();
        if(affRows > 0)
        	return true;

        return false;
	}
	
}
