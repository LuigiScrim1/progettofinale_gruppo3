package com.amministrazione.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.amministrazione.connessione.ConnettoreDB;
import com.amministrazione.model.Utente;

public class UtenteDAO implements Dao<Utente>{

    @Override
    public Utente getById(int id) throws SQLException {
        Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "SELECT id, nominativo, nomeUtente, pass, ruolo, indirizzoSpedizione, indirizzoFatturazione, email FROM Utente WHERE id = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setInt(1, id);

        ResultSet risultato = ps.executeQuery();
        risultato.next();
        Utente temp = new Utente();
        temp.setId(risultato.getInt(1));
        temp.setNominativo(risultato.getString(2));
        temp.setNomeUtente(risultato.getString(3));
        temp.setPass(risultato.getString(4));
        temp.setRuolo(risultato.getString(5));
        temp.setIndirizzoSpedizione(risultato.getString(6));
        temp.setIndirizzoFatturazione(risultato.getString(7));
        temp.setEmail(risultato.getString(8));
        return temp;
        }

    @Override
    public ArrayList<Utente> getAll() throws SQLException{
        ArrayList<Utente> elenco = new ArrayList<Utente>();
        Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "SELECT id, nominativo, nomeUtente, pass, ruolo, indirizzoSpedizione, indirizzoFatturazione, email FROM Utente";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

        ResultSet risultato = ps.executeQuery();

        while(risultato.next()) {

        Utente temp = new Utente();
        temp.setId(risultato.getInt(1));
        temp.setNominativo(risultato.getString(2));
        temp.setNomeUtente(risultato.getString(3));
        temp.setPass(risultato.getString(4));
        temp.setRuolo(risultato.getString(5));
        temp.setIndirizzoSpedizione(risultato.getString(6));
        temp.setIndirizzoFatturazione(risultato.getString(7));
        temp.setEmail(risultato.getString(8));
        elenco.add(temp);
        }

        return elenco;

    }

    @Override
    public void insert(Utente t) throws SQLException {
        Connection conn = ConnettoreDB.getIstanza().getConnessione();

        String query = "INSERT INTO Utente (nominativo, nomeUtente, pass, ruolo, indirizzoSpedizione, indirizzoFatturazione, email) VALUE (?,?,?,?,?,?,?)";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, t.getNominativo());
        ps.setString(2, t.getNomeUtente());
        ps.setString(3, t.getPass());
        ps.setString(4, t.getRuolo());
        ps.setString(5, t.getIndirizzoSpedizione());
        ps.setString(6, t.getIndirizzoFatturazione());
        ps.setString(7, t.getEmail());


        ps.executeUpdate();
        ResultSet risultato = ps.getGeneratedKeys();
        risultato.next();

        t.setId(risultato.getInt(1));
    }


    @Override
    public boolean delete(Utente t) throws SQLException {
        Connection conn = ConnettoreDB.getIstanza().getConnessione();
        String query = "DELETE FROM Utente WHERE id = ?";
        PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
        ps.setInt(1, t.getId());

         int affRows = ps.executeUpdate();
         if(affRows > 0)
            return true;

            return false;
        }

        @Override
        public boolean update(Utente t) throws SQLException {

            Connection conn = ConnettoreDB.getIstanza().getConnessione();
            String query = "UPDATE Utente SET "
                    + "nominativo = ?, "
                    + "nomeUtente = ?, "
                    + "pass = ?, "
                    + "ruolo = ?,"
                    + "indirizzoSpedizione = ?,"
                    + "indirizzoFatturazione = ?,"
                    + "email = ?"
                    + "WHERE id = ?";
            PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
            ps.setString(1, t.getNominativo());
            ps.setString(2, t.getNomeUtente());
            ps.setString(3, t.getPass());
            ps.setString(4, t.getRuolo());
            ps.setString(5, t.getIndirizzoSpedizione());
            ps.setString(6, t.getIndirizzoFatturazione());
            ps.setString(7, t.getEmail());
            ps.setInt(8, t.getId());

            int affRows = ps.executeUpdate();
            if(affRows > 0)
                return true;

            return false;
        }

        public boolean controllaUtente(Utente obj_utente) throws SQLException{

            Connection conn = ConnettoreDB.getIstanza().getConnessione();
            String query = "SELECT nomeUtente, pass, ruolo FROM Utente WHERE nomeUtente = ?";
            PreparedStatement ps =  conn.prepareStatement(query);
            ps.setString(1, obj_utente.getNomeUtente());

            ResultSet risultato = ps.executeQuery();

            boolean risposta = false;

            if(risultato.next()) {
                String passTemp = risultato.getString(2);
                if (passTemp.equals(obj_utente.getPass())) {
                    obj_utente.setRuolo(risultato.getString(3));
                    return true;
                }
            }
            else {
                risposta = false;
            }
            return risposta;
        }
}