package com.amministrazione.utility;

public class ResponsoOperazione{
    private String risultato;
    private String dettaglio;

    public ResponsoOperazione(String risultato, String dettaglio) {
        this.risultato = risultato;
        this.dettaglio = dettaglio;
    }
}