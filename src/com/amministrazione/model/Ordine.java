package com.amministrazione.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Ordine {
	private Integer id;
	private String codiceOrdine;
	private Float totale;
	private LocalDateTime dataOrdine;
	private Integer clientRef;
	private String nomeCliente;
	private ArrayList<Oggetto> array_oggetti = null;
	
	
	public Integer getClientRef() {
		return clientRef;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public void setClientRef(Integer clientRef) {
		this.clientRef = clientRef;
	}

	
	public Ordine() {
		this.array_oggetti = new ArrayList<Oggetto>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodiceOrdine() {
		return codiceOrdine;
	}

	public void setCodiceOrdine(String codiceOrdine) {
		this.codiceOrdine = codiceOrdine;
	}

	public Float getTotale() {
		return totale;
	}

	public void setTotale(Float totale) {
		this.totale = totale;
	}

	public LocalDateTime getDataOrdine() {
		return dataOrdine;
	}

	public void setDataOrdine(LocalDateTime dataOrdine) {
		this.dataOrdine = dataOrdine;
	}

	public ArrayList<Oggetto> getArray_oggetti() {
		return array_oggetti;
	}

	public void setArray_oggetti(ArrayList<Oggetto> array_oggetti) {
		this.array_oggetti = array_oggetti;
	}
	
	public void setOggetti(Oggetto objOggetto) {
		this.array_oggetti.add(objOggetto);
	}
	
	
}
