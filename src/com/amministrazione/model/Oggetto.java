package com.amministrazione.model;

import java.util.ArrayList;

public class Oggetto {
	private Integer id;
	private String nome;
	private String codice;
	private Integer quantita;
	private Float prezzo;
	private String descrizione;
	private ArrayList<Utente> array_store = new ArrayList<Utente>();
	
	
	public Oggetto() {

	}
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public Integer getQuantita() {
		return quantita;
	}
	public void setQuantita(Integer quantita) {
		this.quantita = quantita;
	}
	public Float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(Float prezzo) {
		this.prezzo = prezzo;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public ArrayList<Utente> getArray_store() {
		return array_store;
	}

	public void setArray_store(ArrayList<Utente> array_store) {
		this.array_store = array_store;
	}
	
	public void setUtente(Utente objUtente) {
	    this.array_store.add(objUtente);
	}
	
}
