<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<title>SISL</title>
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-2">
				<img src="pics/Logo.png" alt="NostroLogo"  class="mt-3"  width="250" height="100">
			</div>
			<div class="col-10 text-center">
				<h1> SISL.it</h1> </br>
				<h3>Acquista dai tuoi store preferiti</h3>
			</div>
		</div>
	</div>

	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="#">SISL.it</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
			  <ul class="navbar-nav">
				<li class="nav-item active">
				  <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="login.html"> Login </a>
				</li>
				<li class="nav-item">
				  <a class="nav-link" href="#">Registrati </a>
				</li>
				<li class="nav-item dropdown">
				  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Stores
				  </a>
				  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a class="dropdown-item" href="#" id="store-drpdwn">Store1</a>
					<a class="dropdown-item" href="#" id="store-drpdwn">Store1</a>
					<a class="dropdown-item" href="#" id="store-drpdwn">Store1</a>
				  </div>
				</li>
			  </ul>
			</div>
		  </nav>
	</div>

	<div class="container mt-2">
		<div class="row">
			<div class="col-2" hidden>
				<h5>Seleziona uno store</h5> </br>
				<div class="list-group">
					<a href="#" class="list-group-item list-group-item-action" id="nomestore">STORE1</a>
					<a href="#" class="list-group-item list-group-item-action">STORE2</a>
					<a href="#" class="list-group-item list-group-item-action">STORE3</a>
				</div>
			</div>
			<div class="col-12">
				<div class="container">
					<div class="row">
						<div class="col">
							<h3>Elenco prodotti</h3>
						</div>
					</div>
					<div class="row">
						<div class="col" id="card-index">
							<div class="card-group">
								<div class="card">
								  <img class="card-img-top" src="pics/matebook-x-pro-2020-space-gray.png" alt="Card image cap"  width="250" height="400">
								  <div class="card-body">
									<h5 class="card-title">Matebook</h5>
									<p class="card-text">Contenuto della descrizione proveniente dal DB</p>
								  </div>
								</div>
								<div class="card">
								  <img class="card-img-top" src="pics/lampada.jpg" alt="Card image cap"  width="250" height="400">
								  <div class="card-body">
									<h5 class="card-title">Card title</h5>
									<p class="card-text">Contenuto della descrizione proveniente dal DB</p>
								  </div>
								</div>
								<div class="card">
								  <img class="card-img-top" src="pics/macbook.jpg" alt="Card image cap"  width="250" height="400">
								  <div class="card-body">
									<h5 class="card-title">Macbook pro</h5>
									<p class="card-text">Contenuto della descrizione proveniente dal DB</p>
								  </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>